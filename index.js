const fs = require('fs');

getMonsterList = () => {
    const monsterList = fs.readFileSync("./monster.json", "utf-8");
    return JSON.parse(monsterList);
}

addMonster = (monster) => {
    const monsters = getMonsterList();
    monsters.push(monster);
    console.log(monsters);
    fs.writeFileSync("./monster.json", JSON.stringify(monsters))
}

function getMonsterDetail(name) {
    let monsters = getMonsterList();
    monsters.forEach((monster) => {
        if (monster.name === name) {
            console.log("monster", monster)
        }
    });
}

deleteMonster = (name) => {
    const monsters = getMonsterList();
    let remainingMonster = monsters.filter((monster) => {
        if (monster.name == name) {
            console.log(`${name} has been deleted with detail below`);
            console.log(monster);
        }
        return monster.name !== name;    
    })
    fs.writeFileSync('./monster.json', JSON.stringify(remainingMonster))
}

if(Number(process.argv[2]) === 1) {
    console.log("list monster sekarang")
    console.log(getMonsterList());
}

if(Number(process.argv[2]) === 2) {
    console.log("added monster");
    let i = 6;
    let monster = {
        name: process.argv[3],
        class: process.argv[4],
        type: process.argv[5],
        //please input with following format "'a', 'b', 'c', etc"
        weakness: JSON.parse('"[' + process.argv[6] + ']"' )
        // weakness: eval(process.argv[6])
    }
    addMonster(monster);
}

if(Number(process.argv[2]) === 3) {
    console.log("this is the detail of the monster:");
    getMonsterDetail(process.argv[3]);
}

//to delete monster with name as input
if(Number(process.argv[2]) === 4) {
    deleteMonster(process.argv[3]);
    console.log(`remaining monster on list:`);
    console.log(getMonsterList())
}